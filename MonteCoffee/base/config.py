r""" Module: config.py: function for loading parameters from .cfg (.ini) files """

import sys

if sys.version_info[0]>2:
    import configparser
else:
    import ConfigParser as configparser


def get_parameters(fname='kMC_options.cfg'):
    """ Parse the ini file fname 
        fname: file name of the config file
        ----
        Result:
        dictionary of parameters from the config file 
    """
    
    # Load software configuration
    d = {"SaveSteps": 1000, "LogSteps": 1000, "PicklePrefix": None,
    "SaveCovs": "False", "Verbose": "False", "Nspecies": 2, "Delta": 0.25,
    "Nf": 1000, "Ns": 2000, "Ne": 100, "T": 300, 'tend': 1.0, 'step_end': 1000}
    config = configparser.RawConfigParser(d)
    lsconfigs = config.read(fname)
    
    d['step_end'] = config.getint('Parameters', 'step_end')
    d['tend'] = config.getfloat('Parameters', 'tend')
    d['T'] = config.getfloat('Parameters', 'T')
    d['SaveSteps'] = config.getint('Parameters', 'SaveSteps')
    d['LogSteps'] = config.getint('Parameters', 'LogSteps')
    d['PicklePrefix'] = config.get('Parameters', 'PicklePrefix')
    d['Nspecies'] = config.getint('Parameters', 'Nspecies') # THIS SHOULD BE AUTOMATICALLY DEFINED, not a parameter in the config file

    d['SaveCovs'] = config.getboolean('Options', 'SaveCovs')
    d['Verbose'] = config.getboolean('Options', 'Verbose')

    # Variables connected to temporal acceleration
    # --------------------------------------------------
    d['Delta'] = config.getfloat('Options','Delta') # reversibility tolerance
    d['Nf'] = config.getint('Options','Nf') # Avg event observance in superbasins
    d['Ns'] = config.getint('Options','Ns') # update the barriers every Ns step.
    d['Ne'] = config.getint('Options','Ne') # Nsteps for sufficient executed events.
    d['config'] = config
    return d



