from __future__ import print_function

import numpy as np

from timeit import default_timer as timer

from MonteCoffee.base.kmc import NeighborKMCBase

        
class NeighborKMCSurface(NeighborKMCBase):

    def __init__(self, system, parameters={}):
        r"""Constructor for NeighborKMCSurface objects.
            
            Method calls constructor of NeighborKMCBase objects.
    
            Parameters
            ----------
            system : System instance
                A system instance with defined neighborlists.

            tend : float
                Defines when the simulation has ended.

            parameters : dict
                Parameters used, which are dumped to the log file.
                Example: parameters = {'T':700,'Note':'Test simulation'}

            Returns
            -------
            NeighborKMCSurface instance

        """
        dp = parameters
        self.load_events(dp)
        system.update_ij2occ()
        NeighborKMCBase.__init__(self,system=system,parameters=dp)
        self.step_end = dp['step_end'] if 'step_end' in dp else 1000
        self.tend = dp['tend'] if 'tend' in dp else None
        self.datafile = dp['datafile'] if 'datafile' in dp else 'kmc.dat'
        self.trajectory = dp['trajectory'] if 'trajectory' in dp else None

    def load_events(self, parameters):
        r"""Loads the events list.
    
            User-overridden method.
            
            Method loads the event list 'self.events' which is used to
            keep track of event-types in the simualtion.
    
            Parameters
            ----------
            parameters : dict
                The parameters to pass to the simulation events.

        """
        import pyclbr
        import user_events as ue
        
        self.reverses = {} # Reverse steps # NEEDED IN THE PARENT CLASS, WHY NOT INITIALIZING ALSO THERE?
        self.events = []
        classes = pyclbr.readmodule("user_events")
        event_names = []
        line_nrs = []
        for c in classes:
            if classes[c].file.endswith("user_events.py"):
                event_names.append(c)
                line_nrs.append(classes[c].lineno)

        # Sort events by line number:
        event_names_srt = [event_names[n] for n in np.argsort(line_nrs)] 
        for n in event_names_srt:            
            exec("self.events.append(ue."+n+"(parameters))")

        # Track which steps are considered each others inverse.
        for i in range(len(self.events)): 
            if self.events[i].rev is not None:
                self.reverses[i] = self.events[i].rev

        self.evs_exec = np.zeros(len(self.events))


    def run_kmc(self):
        r"""Runs a kmc simulation.

            Returns
            -------
            0 if simulation is finished.

        """
        import ase.io

        if self.verbose: print('\nRunning simulation...')

        if self.trajectory is not None:
            _a = self.system.get_ase_atoms()
            ase.io.Trajectory(self.trajectory, mode='w', atoms=_a).write()

        df = open( self.datafile, 'w')

        step = 0  # Initialize the main step counter
        while self.t < self.tend:
            self.frm_step()

            if step % self.LogSteps == 0:  # Log every self.LogSteps step.
                if self.verbose:
                    print("{:9d} {:10.4e}".format(step, self.t))

                df.write("{:9d} {:10.4e}".format(step, self.t) )

            if step % self.SaveSteps == 0 and self.trajectory is not None: #Save every self.SaveSteps steps.
                _a = self.system.get_ase_atoms()
                ase.io.Trajectory(self.trajectory, mode='a', atoms=_a).write()

            step+=1
        
        df.close()
        return 0
