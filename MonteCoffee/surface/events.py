r"""
Module: surface/events.py

Module that contains the EventSurfaceDiffusion class.

See also
--------
base/events.py

"""

import ase.io
        
import copy, sys, os
import numpy as np

from MonteCoffee.base.simune_nebtools import SimuneNEBTools
from MonteCoffee.base.events import EventBase
from MonteCoffee.surface.energy import get_n2ij, get_n2occ, n2pown, get_ntype
from MonteCoffee.surface.energy import get_rate_ntype


class EventSurfaceDiffusion(EventBase):
    r"""
    Diffusion event class
    
    The event is X* + * -> * + X*.
    The event is possible if the site is X-covered, and the neighbor site
    is empty. The rate comes from transition state theory. Performing the event
    removes a X from the site, and adds it to the other site.
    """

    def __init__(self, params):
        from MonteCoffee.base.logging import tc
        from MonteCoffee.surface.constants import K2Ha
        EventBase.__init__(self, params)
        
        self.rev    = None # Diffusion is not generally reversible (clustering) 
        self.diffev = True # The event type, right?
        self.T = self.params['T'] * K2Ha

        try:
            self.import_symmetryless_list()
            self.comp_ntype2af_htst(self.ntype2eei, self.ntype2ees)
            self.ntype2occurence = np.zeros(1024, dtype=int)
            
        except:
            
            print(r'{}Could not read the barrier data. Need to compute...{}'.\
                format(tc.FAIL, tc.ENDC))
                
            # ntype2irr, ntype2sym, irr2ntype = self.gen_confs(system)
            # ase.io.write('irr2atoms.traj', self.get_irr2atoms(system, irr2ntype))
    
            # irr2dict = self.comp_ase_nebs(system, irr2ntype)
            # self.export_symmetryless_list(ntype2irr, ntype2sym, irr2ntype, irr2dict)
            # self.import_symmetryless_list()
            # print(self.ntype2irr)
            # print(self.ntype2sop)
            # print(self.ntype2eb)
            # print(self.ntype2er)

               
    def possible(self, sys, si, sf):
        """ If site is covered with an adsorbate and other site free 
            sys: the instance of class System or a derivative of it
            si, sf : site initial and final IDs
        """
        return (sys.sid2occ[si] == 1 and sys.sid2occ[sf] == 0)


    def possibles(self, s, lsif):
        """ If site is covered with an adsorbate and other site free 
            s: the instance of class System or a derivative of it
            lsif: tuple of lists of initial and final site IDs. """
        return np.logical_and( s.sid2occ[lsif[0]]==1, s.sid2occ[lsif[1]]==0)

    def get_rate(self, system, si, sf):
        """ This function will be called even if the event is impossible... """
        ntype = get_ntype(system.sid2ij, system.ij2occ, system.shape, si, sf)
        #print(__name__, n2occ, ntype)
        
        return get_rate_ntype(self.ntype2af, self.ntype2eb, self.T, ntype)

    def do_event(self, system, si, sf):
        """ Updates the event table """
        self.ntype = get_ntype(system.sid2ij, system.ij2occ, system.shape, si, sf)
        self.ntype2occurence[self.ntype] += 1
        
        system.sites[si].covered = 0 # Remove the X from the site
        system.sites[sf].covered = 1 # Add the X to the other site
        system.update_ij2occ_local(si, sf)

    def get_si_sf_n2ij(self, system):
        """
        Auxiary method to find the IDs of initial and final sites and 
        corresponding list of neighbors' coordinates (i,j).
        This method is auxiliary to the calculation of the barriers.
        Returns:
          si, sf : IDs of initial and final sites
          n2ij : list of neighboring sites coordinates 
        """
        assert system.shape[0]>4 and system.shape[1]>3
      
        si, sf = system.ij2sid[1,1], system.ij2sid[2,1]
        n2ij = get_n2ij( system.sid2ij, system.shape, si, sf)
        return si, sf, n2ij


    def gen_confs(self, system):
        """ The method prepares the description of neighborhoods 
          to assist the computation of barriers with NEB method.
          system : the class represinting the surface
          Returns lists:
            ntype2irr: reducible --> irreducible correspondence
            ntype2sym: reducible --> symmetry operation (0 and 1 don't require modif to find barrier, 2 and 3 requre 'direction' change)
            irr2ntype: irreducible configuration --> reducible 
        """
        si, sf, n2ij = self.get_si_sf_n2ij(system)

        sop_n2ij = []           # Symmetry Operation -> n2ij arrays
        sop_n2ij.append( n2ij ) # First symmetry operation is identity

        # Vector helpers for organizing the coordinate transforms        
        v0  = np.array([1,0], dtype=int)
        v1  = np.array([0,1], dtype=int)
        rh = np.array([[1,0],[0,-1]], dtype=int) # reflection over horizontal line
        rv = np.array([[-1,0],[0,1]], dtype=int) # reflection over vertical line
        
        # Apply the reflection over horizontal midline symmetry operation
        m2ij = np.array([np.dot( rh, ij - v1 ) + v1 for ij in n2ij])
        sop_n2ij.append( m2ij ) # Second symmetry operation is reflection across horizontal axis

        # Apply the reflection over vertical midline symmetry operation
        v2ij = np.array([(np.dot( rv, 2*ij - 3*v0 ) + 3*v0)/2.0 for ij in n2ij])
        sop_n2ij.append( v2ij.astype(int) ) # Third symmetry operation is reflection across vertical axis

        # Apply the reflection over horizontal midline after the reflection over the vertical midline 
        m2ij = np.array([np.dot( rh, ij - v1 ) + v1 for ij in v2ij])
        sop_n2ij.append( m2ij.astype(int) ) # Fourth symmetry operation is reflection across vertical after horizontal axis
        
        ij2o = np.zeros((4,3), dtype=int) # Auxiliary (i,j) -> occupation array
        irr2ntype = []
        ntype_seen = set()
        ntype2sym_info = [list() for i in range(1024)]
        
        for ntype in range(1024):
            # If the neighborhood type is not seen, then append to the list of irreducible neighborhoods
            if ntype not in ntype_seen: irr2ntype.append(ntype)

            n2occ = get_n2occ(ntype)

            for m2ij in sop_n2ij:
                for ((i,j), o) in zip(m2ij, n2occ): ij2o[i,j] = o
                n2a = np.array([ij2o[tuple(ij)] for ij in n2ij])
                mtype = (n2pown * n2a).sum()
                ntype_seen.add( mtype )
                ntype2sym_info[ntype].append(mtype)
                

        # Construct correspondence ntype --> symmetry irreducible ntype
        ntype2irr = [0]*1024
        ntype2sym = [0]*1024
        for ntype, inf in enumerate(ntype2sym_info):
            for mtype in inf:
                if mtype in irr2ntype:
                    ntype2irr[ntype] = irr2ntype.index(mtype)
                    ntype2sym[ntype] = inf.index(mtype)
                    break

        return ntype2irr, ntype2sym, irr2ntype


    def get_irr2atoms(self, system, irr2ntype):
        """
        Provides a list of Atoms objects with the generated configurations
        """
        si, sf, n2ij = self.get_si_sf_n2ij(system)
        
        irr2atoms = []
        _s = copy.copy(system.sites)
        for irr, ntype in enumerate(irr2ntype):
            n2occ = get_n2occ(ntype)

            for s in _s: s.coverage = 0 # reset coverage
            for occ, (i,j) in zip(n2occ, n2ij):
                _s[system.ij2sid[i,j]].covered = occ # cover according to ntype
                        
            cslab = system.get_ase_atoms(sites=_s) # get atomistic model
            irr2atoms.append(cslab)

        return irr2atoms
        

    def comp_ase_nebs(self, system, irr2ntype):
        """ 
        Performs calculation of barriers with ASE's NEB implementation.     
        """
        import ase.calculators.emt
        import ase.optimize
        import pprint
        from numpy import array
                
        si, sf, n2ij = self.get_si_sf_n2ij(system)
        
        pwd = os.getcwd()
        
        rootdir = os.path.join( pwd, 'nebs-{}'.format(self.__class__.__name__))
        
        print(rootdir)
        try:
            os.makedirs(rootdir)
        except:
            pass
            
        os.chdir(rootdir)
        
        _s = copy.copy(system.sites)
        _a = system.atoms.copy()
        _a.set_calculator(ase.calculators.emt.EMT())
        dyn = ase.optimize.BFGS(_a, trajectory='relax-surface.traj')
        dyn.run(fmax=0.02)
        
        dh = 0.0
        irr2dict = []
        
        for iconf, ntype in enumerate(irr2ntype):
            wd = os.path.join(rootdir, 'neb-{:08}.dir'.format(iconf))
            wd_exists = os.path.exists(wd)
            
            fname_sum = os.path.join(wd, 'summary.dic')
            sum_exists = os.path.exists(fname_sum)
            
            if sum_exists:
                dic = eval(open(fname_sum, 'r').read())
                irr2dict.append(dic)
                continue
            elif wd_exists:
                print('Composing summary.dic in {}'.format( wd ))
                dic = self.compose_summary_dict(wd)
                irr2dict.append(dic)
                with open(fname_sum,'w') as _fo: pprint.pprint(dic, stream=_fo)
                continue
            
            try:
                os.makedirs(wd)
            except:
                continue
            
            os.chdir(wd)
            print('Computing in {} '.format(wd))
            
            n2occ = get_n2occ(ntype)

            for s in _s: s.covered = 0 # reset coverage
            for occ, (i,j) in zip(n2occ, n2ij):
                _s[system.ij2sid[i,j]].covered = occ # cover according to ntype

            _s[si].covered = 1; _s[sf].covered = 0  # atom in the site si
            cini = system.get_ase_atoms(atoms=_a, sites=_s, dh=dh) # get atomistic model
            atom_si = system.get_adsorbate_atom_id(si, atoms=_a, sites=_s) # get atom id for site si
            
            zbefore = cini[-1].position[2]
            
            _s[si].covered = 0; _s[sf].covered = 1  # atom in the site sf
            cfin = system.get_ase_atoms(atoms=_a, sites=_s, dh=dh) # get atomistic model
            atom_sf = system.get_adsorbate_atom_id(sf, atoms=_a, sites=_s) # get atom id for site sf
            
            assert atom_sf==atom_si
            
            self.comp_ase_neb(cini, cfin, [atom_sf], wd)
            dh = cini[-1].position[2] - zbefore
            print( 'z after neb',  zbefore, cini[-1].position[2], dh)

            dic = self.compose_summary_dict(".")
            irr2dict.append(dic)
            with open(fname_sum,'w') as fout: 
                pprint.pprint(dic, stream=fout)

        os.chdir(pwd)
        return irr2dict



    def comp_ase_neb(self, cini, cfin, lsoma, wd):
        """
        The method performs a single NEB calculation for given initial and
        final configuration of atoms
        cini : initial configuration
        cfin : final configuration
        wd   : working directory 
        lsoma: list of moving atoms
        """
                
        import ase.calculators.emt
        import ase.optimize
        from ase.build import add_adsorbate
        import ase.neb
        import ase.vibrations
    
        cini.set_calculator(ase.calculators.emt.EMT())
        dyn = ase.optimize.BFGS(cini, trajectory='initial.traj', logfile='initial.out')
        dyn.run(fmax=0.02)

        ph = ase.vibrations.Vibrations(cini, indices=lsoma)
        ph.clean()
        sys.stdout = open('vibration_initial.out', 'w')
        ph.run()
        ph.read()
        ph.summary()
        np.savetxt('vibration_initial_energies.txt',  ph.get_energies() )
        sys.stdout = sys.__stdout__


        cfin.set_calculator(ase.calculators.emt.EMT())
        dyn = ase.optimize.BFGS(cfin, trajectory='final.traj', logfile='final.out')
        dyn.run(fmax=0.02)

        ph = ase.vibrations.Vibrations(cfin, indices=lsoma)
        ph.clean()
        sys.stdout = open('vibration_final.out', 'w')
        ph.run()
        ph.read()
        ph.summary()
        np.savetxt('vibration_final_energies.txt',  ph.get_energies() )
        sys.stdout = sys.__stdout__

        images = [cini]
        for i in range(8):
            image = cini.copy()
            image.set_calculator(ase.calculators.emt.EMT())
            images.append(image)
        images.append(cfin)
        
        images[0].get_potential_energy()
        images[-1].get_potential_energy()
        
        neb = ase.neb.NEB(images, k=0.05, climb=True)
        neb.interpolate()
        ase.io.write('before_neb_optimization.traj', images=neb.images)

        fname = 'neb_optimization'
        qn = ase.optimize.MDMin(neb, trajectory=fname+'.traj', logfile=fname+'.out')
        isfinished = qn.run(fmax=0.03, steps=10000)
        isaddle =np.argmax(np.array([i.get_potential_energy() for i in neb.images]))
        spi = neb.images[isaddle] # Saddle-point image
        spi.set_calculator( ase.calculators.emt.EMT() )
        ph = ase.vibrations.Vibrations(spi, indices=lsoma)
        ph.clean()
        sys.stdout = open('vibration_saddle_point.out', 'w')
        ph.run()
        ph.read()
        ph.summary()
        np.savetxt('neb_saddle_vibration_energies.txt',  ph.get_energies() )
        sys.stdout = sys.__stdout__

    def compose_summary_dict(self, dname):
        
        f1 = os.path.join( dname, 'neb_optimization.traj')
        all_images = ase.io.read(f1+'@:') # Load all images from the NEB optimization
        nt = SimuneNEBTools(all_images)
        nmax = nt.get_nmaxima()
        if nmax>1:
            raise RuntimeError('nmax>1: wd = {} nmax = {}'.format(wd, nmax))

        dic = {}
            
        fname = os.path.join( dname, 'neb_saddle_vibration_energies.txt')
        e_ph = np.loadtxt(fname, dtype=complex)[:6]
        nimag = sum(np.imag(e_ph)!=0.0)
        if nimag>1:
            raise RuntimeError('nimag>1: wd = {} nimag = {} fname = {}'.format(
            wd, nimag, fname))
        dic['e_ph_s'] = e_ph
            
        fname = os.path.join( dname, 'vibration_initial_energies.txt')
        e_ph = np.loadtxt(fname, dtype=complex)[:6]
        nimag = sum(np.imag(e_ph)!=0.0)
        if nimag>0:
            raise RuntimeError('nimag>0: wd = {} nimag = {} fname = {}'.format(
            wd, nimag, fname))
        dic['e_ph_i'] = e_ph
            
        fname = os.path.join( dname, 'vibration_final_energies.txt')
        e_ph = np.loadtxt(fname, dtype=complex)[:6]
        nimag = sum(np.imag(e_ph)!=0.0)
        if nimag>0:
            raise RuntimeError('nimag>0: wd = {} nimag = {} fname = {}'.format(
            wd, nimag, fname))
        dic['e_ph_f'] = e_ph
            
        Eb, Er = nt.get_barrier()
         
        dic['E_barrier'] = Eb
        dic['E_reaction'] = Er
        
        return dic

    def export_symmetryless_list(self, ntype2irr, ntype2sym, irr2ntype, irr2dict):
        """
        Compose an easy to load and use in calculations data file
        """
        rd = os.path.join( os.getcwd(), 'nebs-{}'.format(self.__class__.__name__))
        
        assert len(irr2ntype) == len(irr2dict), \
        "Not all calculations are in order? {} {}".format(len(irr2ntype), len(irr2dict))
        
        dat = np.zeros((len(ntype2irr), 13), dtype=complex)
        for ntype, (sop, irr) in enumerate(zip(ntype2sym, ntype2irr)):
            d = irr2dict[irr]
            ebi, eri = d['E_barrier'], d['E_reaction']
            if sop in [0,1]:
                eb, er = ebi, eri
                eei, ees, eef = d['e_ph_i'], d['e_ph_s'], d['e_ph_f']
            elif sop in [2,3]:
                eb, er = ebi - eri, -eri
                eei, ees, eef = d['e_ph_f'], d['e_ph_s'], d['e_ph_i']
            else:
                raise RuntimeError('Unexpected symmetry operation')
            
            dat[ntype,0:4] = (irr, sop, eb, er)
            dat[ntype,4:7] = eei
            dat[ntype,7:10] = ees[0:3]
            dat[ntype,10:] = eef
            
            #print("{:05d} {:05d} {:3d} {:15.10f} {:15.10f} {} {} {}".format( 
            #    ntype, irr, sop, eb, er, eei, ees, eef))
        np.savetxt(os.path.join(rd, 'summary-numpy.dat'), dat)
        return None


    def import_symmetryless_list(self):
        """
        Reads data from previous computations
        """

        cwd, bdname = os.getcwd(), 'nebs-{}'.format(self.__class__.__name__)
        dirs = [ cwd, os.path.dirname(cwd)]
        for dname in dirs:
            fname = os.path.join(os.path.join(dname, bdname), 'summary-numpy.dat')
            print('{}: loading {}'.format(__name__, fname))
            if not os.path.exists(fname): 
                continue
            self.ntype2data = np.loadtxt(fname, dtype=complex)

        self.ntype2irr    = self.ntype2data[:,0].real.astype(int)
        self.ntype2sop    = self.ntype2data[:,1].real.astype(int)
        self.ntype2eb     = self.ntype2data[:,2].real / ase.units.Hartree
        self.ntype2er     = self.ntype2data[:,3].real / ase.units.Hartree
        self.ntype2eei = self.ntype2data[:,4:7] / ase.units.Hartree
        self.ntype2ees = self.ntype2data[:,7:10] / ase.units.Hartree
        self.ntype2eef = self.ntype2data[:,10:13] / ase.units.Hartree


    def check_n2ij(self, system):
        r""" Developer / debugging method to visualize the 
        nearest neighbor barrier neighborhood. """
        import ase.io

        fname = 'check_edges_n2ij.traj'
        focc = 0
        for si, site in enumerate(system.sites):            
            print(si, system.sid2ij[si])
            print()

            for lnid, sf in enumerate(site.neighbors):
                print(lnid, sf, system.sid2ij[sf])
                system.uncover_system()

                system.sites[si].covered = 1
                system.sites[sf].covered = 0
            
                n2ij = get_n2ij( system.sid2ij, system.shape, si, sf )
                for (i,j) in n2ij:
                    system.sites[system.ij2sid[i,j]].covered = 1

                _a = system.get_ase_atoms()
                m = 'a' if focc>0 else 'w'
                ase.io.Trajectory(fname, mode=m, atoms=_a).write()
                focc += 1
            
        print('ase gui {}'.format(fname))
        sys.exit(0)

    def comp_ntype2af_htst(self, ntype2eei, ntype2ees):
        """ Computes the attempt frequencies from the harmonic transition state theory.
            ntype2eei: Numpy's array of vibration frequencies at the minimum (Hartree atomic units)
            ntype2ees: Numpy's array of vibration frequencies at the saddle point (Hartree atomic units)
        Returns:
            ntype2af : attempt frequencies (in Hz = s^-1 units)
        """
        assert np.all(ntype2eei.shape==ntype2ees.shape)
        
        self.ntype2af = nt2a = np.zeros(len(ntype2eei))
        for nt, (eei, ees) in enumerate(zip(ntype2eei, ntype2ees)):
            nt2a[nt] = (np.prod(eei.real) / np.prod(ees[1:].real)) * 6579.683879634054e12
        return nt2a
                    
    def compute_htst(self, system):
        """ Compute the activation energies and the frequencies of normal 
        vibrations for the nearest-neighbor, rectangular reaction site"""
        
        print('Run NEB and Vibrations calculations...', __name__, self)
        ntype2irr, ntype2sym, irr2ntype = self.gen_confs(system)
        ase.io.write('irr2atoms.traj', self.get_irr2atoms(system, irr2ntype))
    
        irr2dict = self.comp_ase_nebs(system, irr2ntype)
        self.export_symmetryless_list(ntype2irr, ntype2sym, irr2ntype, irr2dict)
        self.import_symmetryless_list()


    def generate_pics(self, system):
        """ Generate the pictures """
        import matplotlib as mpl
        mpl.use('Agg')
        import matplotlib.pyplot as plt
        from matplotlib import gridspec
        from ase.visualize.plot import plot_atoms
        
        ntype2irr, ntype2sym, irr2ntype = self.gen_confs(system)
        irr2atoms = self.get_irr2atoms(system, irr2ntype)
        ase.io.write('irr2atoms.traj', irr2atoms)
         
        for irr, (atoms, ntype) in enumerate(zip(irr2atoms, irr2ntype)):
            fig = plt.figure(1, figsize=(5,3))
            gs = gridspec.GridSpec(1,1)
            ax = fig.add_subplot(gs[0,0])
            ax.tick_params(bottom=False, top=False, left=False, right=False)
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            a2z = atoms.get_atomic_numbers()
            cc = ['red' if z==a2z[-1] else 'lightgray' for z in a2z]
            plot_atoms(atoms, ax, colors=cc)
            plt.tight_layout()
            fname = "{:05d}.png".format(irr)
            print(fname)
            fig.savefig(fname)
            plt.close()

    def print_irr2ntype(self, system):
        """ Generate the pictures """
        ntype2irr, ntype2sym, irr2ntype = self.gen_confs(system)
         
        for irr, ntype in enumerate(irr2ntype):
            print("{: 5d} & {: 5d} & ".format( irr, ntype))

