from __future__ import print_function, division


import sys, glob
import numpy as np

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec
import MonteCoffee.base.config

prm = MonteCoffee.base.config.get_parameters()
T = prm['T']
print( "Temperature T = {} K".format( T ))

lsdat = glob.glob('kmc-*.dat')
tend = np.loadtxt(lsdat[0])[-1,1]

dd = []
tt = []
for fname in lsdat:
    d = np.loadtxt(fname)
    if len(d)<1: continue
    if abs(d[-1,1]-tend)>abs(d[-1,1]+tend)*1e-2: 
        print('skipped: {} {} {}'.format( fname, abs(d[-1,1]-tend), (d[-1,1]+tend)*1e-4 ))
        continue
        
    tt.append( d[-1,1] )
    dd.append( d[-1,2] )
    #plt.plot(d[:,1], d[:,3], label=fname)
    #plt.plot(d[:,0], d[:,1], label=fname)

dd = np.array(dd)

print( sum(dd) / len(dd) / tt[-1])
print( len(dd) )

Ds = np.zeros(len(dd))
psum = 0.0
for n, d in enumerate(dd):
    psum += d
    Ds[n] = psum / (n+1) / tt[-1]

a0 = 2.772e-10      # (m)
af = 2.35685996e+12 # (s^-1)
ae = 3.95448859e-01 # (eV)
ev2K = 11604.58857702
exp_mEdT = np.exp(-ae * ev2K / T )


dim = 2
Ds *= (2.772e-10)**2 / (2.0 * dim) /     exp_mEdT


print("min(dd) = {}, max(dd) = {}".format( min(dd), max(dd)) )

print( 'D_0 = {:10.4e} (m^2 / s) (analytic)'.format( a0**2 * af) ) 
print( 'D   = {:10.4e} (m^2 / s) at T = {:5.1f} K (analytic)'.format( \
    a0**2 * af * np.exp( -ae * ev2K / T), T ) ) 

print( 'D_0 = {:10.4e} (m^2 / s)'.format( Ds[-1] ))
print( 'D   = {:10.4e} (m^2 / s) at T = {:5.1f} K'.format(Ds[-1] * exp_mEdT, T) )

plt.rc('text', usetex=True)
mpl.rc('font', **{'size': 14})
lfs = 18
fig = plt.figure(1, figsize=(8,4))
gs = gridspec.GridSpec(1,2)

### a ##################################################################
ax = fig.add_subplot(gs[0,0])

ax.text(0.9, 0.9, "a)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

ax.plot(dd)
ax.set_xlabel(r'Indexing number of simulation')
ax.set_ylabel(r'Square distance (steps$^2$)')

### b ##################################################################
ax = fig.add_subplot(gs[0,1])

ax.text(0.9, 0.9, "b)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

ax.plot(Ds)
ax.set_xlabel(r'Number of kMC simulations, $N$')
ax.set_ylabel(r'Diffusion coefficient, $D_0$ (m$^2$ / s)')
#ax.set_xlim((50,len(dd)))
#ax.legend()


fig.tight_layout()
fig.savefig('plot.pdf')
plt.show()
