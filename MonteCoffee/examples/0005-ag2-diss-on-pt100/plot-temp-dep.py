from __future__ import print_function, division

import os, sys, glob
import numpy as np

def get_data_dir(dname):
    """ Reads a set of directories """
    import MonteCoffee.base.config
    
    fname = os.path.join(dname, 'kMC_options.cfg')
    try :
        prm = MonteCoffee.base.config.get_parameters(fname=fname)
    except:
        return {}
        
    T = prm['T']
    
    lsdat = glob.glob(os.path.join(dname, 'kmc-*.dat'))
    ss = []
    tt = []
    for fname in lsdat:
        #print(fname)
        d = np.loadtxt(fname)
        if len(d)<1:
            print('skipped: {}'.format( fname))
            continue
       
        ss.append(  int(d[-1,0]) )
        tt.append(  d[-1,1] )

    tt = np.array(tt)
    ss = np.array(ss, dtype=int)
    return {'T': T,'tt': tt,'ss': ss}


if __name__ == '__main__':
    import matplotlib as mpl
    #mpl.use('Agg')
    import matplotlib.pyplot as plt
    from matplotlib import gridspec

    TT = []
    mtt = []
    mss = []
    for dname in glob.glob("*K.dir"):
        d = get_data_dir(dname)
        if 'T' not in d: 
            continue
        
        tt,ss,T = d['tt'], d['ss'], d['T']
        TT.append(T)
        mtt.append( tt.sum() / len(tt) )
        mss.append( ss.sum() / len(ss) )
        print(dname, T, len(tt), mtt[-1], mss[-1])

plt.rc('text', usetex=True)
mpl.rc('font', **{'size': 14})
lfs = 18
fig = plt.figure(1, figsize=(8,4))
gs = gridspec.GridSpec(1,2)

### a ##################################################################
ax = fig.add_subplot(gs[0,0])

ax.text(0.9, 0.9, "a)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

mtt = np.array(mtt)
TT  = np.array(TT)

ax.semilogy(TT, mtt, "o-")
ax.set_xlabel(r'Temperature $T$ (K)')
ax.set_ylabel(r'Mean dissociation time $\tau_2$ (s)')

### a ##################################################################
ax = fig.add_subplot(gs[0,1])

ax.text(0.9, 0.9, "b)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

mss = np.array(mss)

ax.semilogy(TT, mss, "o-")
ax.set_xlabel(r'Temperature $T$ (K)')
ax.set_ylabel(r'Number of steps to dissociation')

fig.tight_layout()
fig.savefig('plot-temp-dep.pdf')
print('plot-temp-dep.pdf')
plt.close()

fig = plt.figure(1, figsize=(8,4))
gs = gridspec.GridSpec(1,2)
### b ##################################################################
ax = fig.add_subplot(gs[0,0])

ax.text(0.9, 0.9, "a)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

ax.plot(np.log(TT/min(TT)), np.log(mtt / max(mtt)), "o-")
ax.plot(np.log(TT[0:3]/min(TT)), np.log ( max(mtt)*np.exp(-36)**np.log(TT[0:3]/min(TT))/ max(mtt)), "-")
ax.set_xlabel(r'$\log(T/T_{\min})$')
ax.set_ylabel(r'$\log(\tau_2/ \tau_{\max})$')

### c ##################################################################
ax = fig.add_subplot(gs[0,1])

ax.text(0.9, 0.9, "b)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

ax.plot(np.log(TT/max(TT)), np.log(mtt / min(mtt)), "o-")
ax.plot(np.log(TT[-7:]/max(TT)), np.log ( min(mtt)*np.exp(-6.73)**np.log(TT[-7:]/max(TT))/ min(mtt)), "-")
ax.set_xlabel(r'$\log(T/T_{\max})$')
ax.set_ylabel(r'$\log(\tau_2/ \tau_{\min})$')

fig.tight_layout()
fig.savefig('plot-temp-dep-log.pdf')
print('plot-temp-dep-log.pdf')
#plt.show()


