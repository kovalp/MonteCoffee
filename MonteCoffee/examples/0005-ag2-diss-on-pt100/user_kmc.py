from __future__ import print_function

import sys, numpy as np

from MonteCoffee.surface.kmc import NeighborKMCSurface

class NeighborKMC(NeighborKMCSurface):

    def __init__(self, system, parameters={}):
        r"""Constructor for NeighborKMC objects.
            
            Method calls constructor of NeighborKMCSurface. 
    
            Parameters
            ----------
            system : System instance
                A system instance with defined neighborlists.

            parameters : dict
                Parameters used, which are dumped to the log file.
                Example: parameters = 
                {'T':700,'Note':'Test simulation'}

            Returns
            -------
            NeighborKMC instance
        """
        import ase.units
        
        dp = parameters
        NeighborKMCSurface.__init__(self, system=system, parameters=dp)
        
        self.occurencefile = dp['occurencefile'] if 'occurencefile' in dp else None
        
        #T = dp['T']
        #print('Temperature {:10.8e} (K)'.format( T ))
        #ntypes = [2,1,16]
        #irr = [self.events[0].ntype2irr[nt] for nt in ntypes]
        #aff = [self.events[0].ntype2af[nt] for nt in ntypes]
        #eaa = [self.events[0].ntype2eb[nt]*ase.units.Hartree for nt in ntypes]

        #print(('{:2s}'+' {:14s}'*3).format(' I', 'Af (s^-1)', 'E_a (eV)', 'k (s^-1)'))
        #for ir, af, ea in zip(irr, aff, eaa):
        #    k = af * np.exp(-ea * 11604.58857702 / T)
        #    print('{:2d} {:10.8e} {:10.8e} {:10.8e}'.format(ir, af, ea, k))
        
                
    def run_kmc(self):
        r"""Runs a kmc simulation.

            Returns
            -------
            0 if simulation is finished.

        """
        import ase.io
        from timeit import default_timer as timer
        from MonteCoffee.base.kmc import wt
        
        step = 0                  # Initialize the main step counter

        if self.verbose: 
            print('\nRunning simulation...')

        if self.trajectory is not None:
            _a = self.system.get_ase_atoms()
            ase.io.Trajectory(self.trajectory, mode='w', atoms=_a).write()

        df = open( self.datafile, 'w')

        shape = self.system.shape
        cc = np.array(np.where(self.system.ij2occ>0)).T
        assert cc.shape == (2,2)
        
        x,y = cc[1,0]-cc[0,0], cc[1,1]-cc[0,1]
        x2 = min( abs(x-shape[0]), abs(x), abs(x+shape[0]))**2
        y2 = min( abs(y-shape[1]), abs(y), abs(y+shape[1]))**2
        d2 = x2+y2
        assert d2<3

        fmt = "{: 5d} {: 10.4e} {: 7d} \n"
        while self.t < self.tend:
            self.frm_step()

            cc = np.array(np.where(self.system.ij2occ>0)).T
            assert cc.shape == (2,2)        
            x,y = cc[1,0]-cc[0,0], cc[1,1]-cc[0,1]
            x2 = min( abs(x-shape[0]), abs(x), abs(x+shape[0]))**2
            y2 = min( abs(y-shape[1]), abs(y), abs(y+shape[1]))**2
            d2 = x2+y2
            if d2>5:
                break

            if step % self.LogSteps == 0:
                df.write( fmt.format(step, self.t, d2) )
                if self.verbose:
                    print( "{:9d} {:10.4e} {}".format(step, self.t, d2) )

            if step % self.SaveSteps == 0 and self.trajectory is not None: #Save every self.SaveSteps steps.
                _a = self.system.get_ase_atoms()
                ase.io.Trajectory(self.trajectory, mode='a', atoms=_a).write()

            step+=1


        if self.verbose:
            print( "{:9d} {:10.4e} {}".format(step, self.t, d2) )
       
        df.write( fmt.format(step, self.t, d2) )
        df.close()

        np.savetxt(self.occurencefile, self.events[0].ntype2occurence, fmt='%15u', )

        if self.trajectory is not None: #Save final configuration
            _a = self.system.get_ase_atoms()
            ase.io.Trajectory(self.trajectory, mode='a', atoms=_a).write()

        return 0
