from __future__ import print_function, division

import sys, glob
import numpy as np

import ase.build, ase.io

from MonteCoffee.base.config import get_parameters
from MonteCoffee.base.sites import SiteBase
from MonteCoffee.surface.system import SystemSurface

from user_kmc import NeighborKMC

# Define the system
atoms = ase.build.fcc100('Pt', (6,6,2))
ase.io.write('surface_base.traj', atoms)
print('{}: written surface_base.traj'.format( __name__))

surf_atom_ids = [i for i,a in enumerate(atoms) if a.tag==2] # Adsorption occurs in the hollow positions

# Create a site for each surface-atom:
sites = [None]*len(surf_atom_ids)
for i, atom_id in enumerate(surf_atom_ids):
    sites[i] = SiteBase(stype=1, ind = [atom_id]) 

print('{}: list of Sites created.'.format( __name__))

nkmc_done = len( glob.glob('kmc-*.dat') )
nkmc_todo = nkmc_done + 1000
for ikmc in range(nkmc_done,nkmc_todo):
    system = SystemSurface(atoms=atoms, sites=sites, coverage = 0.0 )
    print('{}: system {} of {} is initialized.'.format( __name__, ikmc, nkmc_todo))
    system.cover_system(coverage=0.0)
    system.cover_single_site(at=(2,2))
    system.cover_single_site(at=(3,2))
    
    prm = { 'trajectory': None, # 'kmc-{:05d}.traj'.format(ikmc),
            'datafile': 'kmc-{:05d}.dat'.format(ikmc),
            'occurencefile': 'kmc-{:05d}.ocr'.format(ikmc)  }
    prm.update( get_parameters())

    sim = NeighborKMC(system=system, parameters=prm)
    sim.run_kmc()

