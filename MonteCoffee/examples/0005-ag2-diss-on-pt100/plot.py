from __future__ import print_function, division


import os, sys, glob
import numpy as np
from pathlib import Path

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec

import ase.build, ase.io

from MonteCoffee.base.config import get_parameters
from MonteCoffee.base.sites import SiteBase
from MonteCoffee.surface.system import SystemSurface
from MonteCoffee.surface.constants import K2Ha
import MonteCoffee.base.config
from user_kmc import NeighborKMC

# Define the system
atoms = ase.build.fcc100('Pt', (6,6,2))
satom_ids = [i for i,a in enumerate(atoms) if a.tag==2] # Adsorption occurs in the hollow positions
sites = [None]*len(satom_ids)
for i,atom_id in enumerate(satom_ids):sites[i]=SiteBase(stype=1,ind=[atom_id])
system = SystemSurface(atoms=atoms, sites=sites, coverage = 1.0 )
sim = NeighborKMC(system=system, parameters={"T": 300})

prm = MonteCoffee.base.config.get_parameters()
T = prm['T']
kT_Ha = T*K2Ha

lsdat = glob.glob('kmc-*.dat')
ss = []
tt = []
for fname in lsdat:
    #print(fname)
    d = np.loadtxt(fname)
    if len(d)<1:
        print('skipped: {}'.format( fname))
        continue
       
    ss.append(  int(d[-1,0]) )
    tt.append(  d[-1,1] )
    

lsocr = glob.glob('kmc-*.ocr')
n2ocr = np.zeros(1024, dtype=int)
for fname in lsocr:
    ocr = np.asarray( np.loadtxt(fname), dtype=int)
    if len(ocr)<1:
        print('skipped: {}'.format( ocrfname))
        continue
    n2ocr += ocr

n2i = sim.events[0].ntype2irr
n2f = sim.events[0].ntype2af
n2b = sim.events[0].ntype2eb

print( 'Occured events')
nevents = n2ocr.sum()
for n in np.where(n2ocr>0)[0]:
    print( "{:3d} {:3d} {:10d} {:15.10e} {:5.4f} %".format(n, n2i[n], n2ocr[n], \
    n2f[n]*np.exp(-n2b[n]/kT_Ha), n2ocr[n]*100/nevents) )


tt = np.array(tt)
ss = np.array(ss, dtype=int)

print( "Temperature T = {} K".format( T ))
print( "Number of kMC simulations {} ".format(len(tt) ))

print( "Mean dissociation time {} (s)".format(sum(tt) / len(tt)))
print("min(tt) = {}, max(tt) = {}".format( min(tt), max(tt)) )

print( "Mean number of steps {}".format(sum(ss) / len(ss)))
print("min(ss) = {}, max(ss) = {}".format( min(ss), max(ss)) )

mdts = np.zeros(len(tt))
psum = 0.0
for n, t in enumerate(tt):
    psum += t
    mdts[n] = psum / (n+1)

ev2K = 11604.58857702

plt.rc('text', usetex=True)
mpl.rc('font', **{'size': 14})
lfs = 18
fig = plt.figure(1, figsize=(8,4))
gs = gridspec.GridSpec(1,2)

### a ##################################################################
ax = fig.add_subplot(gs[0,0])

ax.text(0.9, 0.9, "a)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

ax.plot(tt)
ax.set_xlabel(r'Indexing number of simulation')
ax.set_ylabel(r'Dissociation time (s)')

### b ##################################################################
ax = fig.add_subplot(gs[0,1])

ax.text(0.9, 0.9, "b)", color="black", transform=ax.transAxes, fontsize=lfs)
ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

ax.plot(mdts)
ax.set_xlabel(r'Number of kMC simulations, $N$')
ax.set_ylabel(r'Mean dissociation time (s)')
#ax.set_xlim((50,len(dd)))
#ax.legend()


fig.tight_layout()
fig.savefig('plot.pdf')
plt.show()
