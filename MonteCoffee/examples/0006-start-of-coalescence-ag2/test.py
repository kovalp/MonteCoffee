from __future__ import print_function, division

import sys, glob
import numpy as np

import ase.build, ase.io

from MonteCoffee.base.config import get_parameters
from MonteCoffee.base.sites import SiteBase
from MonteCoffee.surface.system import SystemSurface

from user_kmc import NeighborKMC

cfg = get_parameters()
lattsize = cfg['config'].getint('Parameters', 'lattsize')

natoms_ads = 2
coverage = natoms_ads / lattsize**2 * 100

# Define the system
atoms = ase.build.fcc100('Pt', (lattsize,lattsize,2))
ase.io.write('surface_base.traj', atoms)
print('{}: written surface_base.traj'.format( __name__))

surf_atom_ids = [i for i,a in enumerate(atoms) if a.tag==2] # Adsorption occurs in the hollow positions

# Create a site for each surface-atom:
sites = [None]*len(surf_atom_ids)
for i, atom_id in enumerate(surf_atom_ids):
    sites[i] = SiteBase(stype=1, ind = [atom_id]) 

print('{}: list of Sites created.'.format( __name__))

system = SystemSurface(atoms=atoms, sites=sites, natoms_ads = natoms_ads )

prm = { 'trajectory': None, # 'kmc-{:05d}.traj'.format(T),
        'datafile': 'kmc.dat', 'occurencefile': 'kmc.ocr',
        'tend': 1e100}

prm.update(cfg)

print('{}: Coverage = {} %, T = {}'.format(__name__, coverage, prm['T']) )

sim = NeighborKMC(system=system, parameters=prm)
sim.run_kmc()

