from __future__ import print_function, division

import os, sys, glob
import numpy as np
import scipy.interpolate 

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec

from MonteCoffee.base.config import get_parameters

dirs = glob.glob('*.dir')

TT, lts, covs, dperc = [], [], [], []
for dname in dirs:
    #print(dname)
    with open(os.path.join(dname, 'kmc.dat'), 'r') as f: ll = f.readlines()
    if len(ll)>0:
        d = get_parameters(os.path.join(dname, 'kMC_options.cfg'))
        TT.append( d['config'].getfloat('Parameters', 'T') )
        lts.append( d['config'].getint('Parameters', 'lattsize') )
        covs.append(2.0*100/lts[-1]**2)
        dperc.append(float(ll[-1].split()[-1]))


Xi, Yi = np.meshgrid( np.linspace(min(TT), max(TT), 1000),
                      np.linspace(min(covs), max(covs), 1000) )
Zi = scipy.interpolate.griddata((TT, covs), dperc, (Xi,Yi), method='cubic') # Interpolation

print(np.unique(np.around(lts, decimals=6)))
print(np.unique(np.around(covs, decimals=6)))

plt.rc('text', usetex=True)
mpl.rc('font', **{'size': 14})
lfs = 18
fig = plt.figure(1, figsize=(4,4))
gs = gridspec.GridSpec(1,1)

### a ##################################################################
ax = fig.add_subplot(gs[0,0])

ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)

extent=[min(TT), max(TT), min(covs), max(covs)]

cs = ax.contour(Zi, colors=['red'], alpha=0.5)
ax.clabel(cs, inline=1, fontsize=10)

img = ax.imshow(Zi)

ax.set_xlim(min(TT), max(TT))
#ax.set_ylim(min(covs), max(covs))
ax.set_xlabel(r'Temperature $T$ (K)')
ax.set_ylabel(r'Coverage $\zeta$ (\%)')
plt.colorbar(mappable=img, orientation='vertical', shrink=0.88)

fig.tight_layout()
fig.savefig('plot-denplot.pdf')
plt.show()
