from __future__ import print_function, division

import os, sys, glob
import numpy as np
import scipy.interpolate 

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec

from MonteCoffee.base.config import get_parameters

dirs = glob.glob('*.dir')

TT, lts, covs, dperc = [], [], [], []
for dname in dirs:
    #print(dname)
    with open(os.path.join(dname, 'kmc.dat'), 'r') as f: ll = f.readlines()
    if len(ll)>0:
        d = get_parameters(os.path.join(dname, 'kMC_options.cfg'))
        TT.append( d['config'].getfloat('Parameters', 'T') )
        lts.append( d['config'].getint('Parameters', 'lattsize') )
        covs.append(2.0*100/lts[-1]**2)
        dperc.append(float(ll[-1].split()[-1]))

covs = np.array(covs)
dperc = np.array(dperc)
TT = np.array(TT)

lts_ru = np.unique(np.around(lts, decimals=6))

l2tp = []*len(lts)


plt.rc('text', usetex=True)
mpl.rc('font', **{'size': 14})
lfs = 18
fig = plt.figure(1, figsize=(5,5))
gs = gridspec.GridSpec(1,1)

### a ##################################################################
ax = fig.add_subplot(gs[0,0])

ax.minorticks_on()
ax.tick_params(bottom=True, top=True, left=True, right=True, which='both', direction='in', length=6.0)
ax.tick_params(which='minor', length=3.0)



cc = ['firebrick', 'red',       'sienna',   'maroon',      'orangered',
      'olivedrab', 'darkgreen', 'seagreen', 'darkcyan',    'deepskyblue', 
      'royalblue', 'navy',      'blue',     'darkorchid',  'grey', 
      'black', 'brown']

cc = ['firebrick', 'brown', 'darkgreen', 'black', 'deepskyblue', 
      'blue', 'darkorchid',  'grey', 'black', 'brown']

lss = [ '-', '--', '-.', ':',
       'o-', 'v--', '>-.', '<:',
       's-', 'p--', '*-.', 'h:']

for s,c,l in zip(lss, cc, lts_ru[0:13:2]):
    wh = np.where(l==np.around(lts, decimals=6))[0]
    cv, pe, tt = covs[wh], dperc[wh], TT[wh]
    ax.plot(tt, 100-pe, s, color=c, label='{:5.2f}'.format(cv[0]) )

ax.set_xlim(min(TT), max(TT))
ax.set_ylim(0.0, 100.0)
ax.set_xlabel(r'Temperature $T$ (K)')
ax.set_ylabel(r'Time in bonded state (\%)')

fig.legend(loc=(0.68, 0.53))

fig.tight_layout()
fig.savefig('plot.pdf')
#plt.show()
